package amazon

type Message struct {
	LogType      string `json:"log_type"`
	GuildID      string `json:"guild_id"`
	ServerID     string `json:"server_id"`
	ServerName   string `json:"server_name"`
	NitradoToken string `json:"nitrado_token"`
}
