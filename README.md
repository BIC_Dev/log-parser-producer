# Log Parser Producer

## SQS Commands (localstack)
[Offical Docs](https://docs.localstack.cloud/aws/sqs/)

### Get Queue Attributes
```sh
awslocal sqs get-queue-attributes --queue-url http://localhost.localstack.cloud:4566/000000000000/server-logs --attribute-names All
```

### Create Queue
```sh
awslocal sqs create-queue --queue-name server-logs
```

### Send Message To Queue
```sh
awslocal sqs send-message --queue-url http://localhost.localstack.cloud:4566/00000000000/server-logs --message-body $JSON
```

### List Queues
```sh
awslocal sqs list-queues
```