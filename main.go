package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/caarlos0/env"
	"gitlab.com/BIC_Dev/log-parser-producer/services/amazon"
	"gitlab.com/BIC_Dev/log-parser-producer/services/guildconfigservice"
	"gitlab.com/BIC_Dev/log-parser-producer/utils/logging"
	"go.uber.org/zap"
)

type EnvVars struct {
	Environment               string `env:"ENVIRONMENT,required"`
	AWSRegion                 string `env:"AWS_REGION,required"`
	AWSEndpoint               string `env:"AWS_ENDPOINT,required"`
	SQSQueueURL               string `env:"SQS_QUEUE_URL,required"`
	GuildConfigClientHost     string `env:"GUILD_CONFIG_CLIENT_HOST,required"`
	GuildConfigClientBasePath string `env:"GUILD_CONFIG_CLIENT_BASE_PATH,required"`
	GuildConfigServiceToken   string `env:"GUILD_CONFIG_SERVICE_TOKEN,required"`
	GuildService              string `env:"GUILD_SERVICE,required"`
}

func main() {
	ctx := context.Background()
	envVars := EnvVars{}
	if err := env.Parse(&envVars); err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err), zap.String("error_message", "Failed to parse environment variables"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	local := envVars.Environment == "local"
	sess := amazon.NewSession(envVars.AWSRegion, envVars.AWSEndpoint, 1, local)
	sqsClient := amazon.NewSQSClient(sess)

	att, attErr := sqsClient.GetQueueAttributes(&sqs.GetQueueAttributesInput{
		AttributeNames: []*string{aws.String("ApproximateNumberOfMessages")},
	})
	if attErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", attErr), zap.String("error_message", "Failed to get SQS attribute: ApproximateNumberOfMessages"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	if val, ok := att.Attributes["ApproximateNumberOfMessages"]; ok {
		if *val != "0" {
			ctx := logging.AddValues(ctx, zap.String("message", fmt.Sprintf("SQS is not empty and will stop execution. Queue length: %s", *val)))
			logger := logging.Logger(ctx)
			logger.Info("info_log")

			os.Exit(0)
		}
	}

	gc := guildconfigservice.InitService(ctx, envVars.GuildConfigClientHost, envVars.GuildConfigClientBasePath, envVars.GuildConfigServiceToken)

	// GET ALL GUILDS
	// GET SERVERS FROM GUILD CONFIG SERVICE
	guilds, gErr := guildconfigservice.GetAllGuilds(ctx, gc)
	if gErr != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", gErr.Err), zap.String("error_message", gErr.Message))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	if guilds.Payload == nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", errors.New("guild payload is nil")), zap.String("error_message", "All guilds payload is nil"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	if len(guilds.Payload.Guilds) == 0 {
		ctx = logging.AddValues(ctx, zap.NamedError("error", errors.New("guild payload guilds is empty")), zap.String("error_message", "All guilds payload has no guilds"))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	var totalMessagesSent int
	var totalMessagesFailed int

	// ITERATE THROUGH GUILDS TO GET GUILD FEED
	for _, guild := range guilds.Payload.Guilds {
		// Skip guilds that are not enabled
		if !guild.Enabled {
			continue
		}

		feed, fErr := guildconfigservice.GetGuildFeed(ctx, gc, guild.ID)
		if fErr != nil {
			fCtx := logging.AddValues(ctx, zap.NamedError("error", fErr), zap.String("error_message", "Failed to retrieve guild feed"), zap.String("guild_id", guild.ID))
			logger := logging.Logger(fCtx)
			logger.Error("error_log")
			continue
		}

		if vErr := guildconfigservice.ValidateGuildFeed(feed, envVars.GuildService, "Servers"); vErr != nil {
			fCtx := logging.AddValues(ctx, zap.String("message", "Skipping guild as it has no servers"), zap.String("guild_id", guild.ID))
			logger := logging.Logger(fCtx)
			logger.Info("info_log")
			continue
		}

		// ITERATE THROUGH SERVERS TO SEND TO QUEUE
		for _, server := range feed.Payload.Guild.Servers {
			// Skip server if it's not enabled
			if !server.Enabled {
				continue
			}

			if server.NitradoToken == nil || server.NitradoToken.TokenValue == "" {
				ntCtx := logging.AddValues(ctx, zap.String("message", "Server missing nitrado token"), zap.String("guild_id", guild.ID), zap.Uint64("server_id", server.ID))
				logger := logging.Logger(ntCtx)
				logger.Info("info_log")
				continue
			}

			var logType string
			switch server.ServerTypeID {
			case "arkps":
				logType = "ark1"
			case "arkxb":
				logType = "ark1"
			default:
				continue
			}

			if logType == "" {
				continue
			}

			message := amazon.Message{
				LogType:      logType,
				GuildID:      server.GuildID,
				ServerID:     fmt.Sprint(server.NitradoID),
				ServerName:   server.Name,
				NitradoToken: server.NitradoToken.TokenValue,
			}

			jsonString, jssErr := json.Marshal(message)
			if jssErr != nil {
				jsCtx := logging.AddValues(ctx, zap.NamedError("error", jssErr), zap.String("error_message", "Failed to marshal json for sqs message"), zap.String("guild_id", guild.ID), zap.Uint64("server_id", server.ID))
				logger := logging.Logger(jsCtx)
				logger.Error("error_log")
				continue
			}

			_, smErr := sqsClient.SendMessage(&sqs.SendMessageInput{
				MessageBody: aws.String(string(jsonString)),
				QueueUrl:    aws.String(envVars.SQSQueueURL),
			})
			if smErr != nil {
				smCtx := logging.AddValues(ctx, zap.NamedError("error", smErr), zap.String("error_message", "Failed to send message to SQS"), zap.String("guild_id", guild.ID), zap.Uint64("server_id", server.ID))
				logger := logging.Logger(smCtx)
				logger.Error("error_log")

				totalMessagesFailed++
				continue
			} else {
				smCtx := logging.AddValues(ctx, zap.String("message", "Successfully sent message to SQS"), zap.String("guild_id", guild.ID), zap.Uint64("server_id", server.ID))
				logger := logging.Logger(smCtx)
				logger.Info("info_log")

				totalMessagesSent++
			}
		}
	}

	doneCtx := logging.AddValues(ctx, zap.String("message", "Finished writing messages to SQS"), zap.Int("total_messages_sent", totalMessagesSent), zap.Int("total_messages_failed", totalMessagesFailed))
	logger := logging.Logger(doneCtx)
	logger.Info("info_log")

	os.Exit(0)
}
